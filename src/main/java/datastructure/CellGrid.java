package datastructure;

import java.util.ArrayList;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int cols;
    private int rows;
    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
        if(rows <= 0 || columns <= 0){
            throw new IllegalArgumentException();
        }

		this.cols = columns;
        this.rows = rows;
        this.grid = new CellState[rows][columns];
        
        for (int row = 0; row < rows; row++){
            for (int col = 0; col < cols; col++){
               this.grid[row][col] = initialState;  
            }
        }
            
    }

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row < 0 || row >= rows || column < 0 || column >= cols){
            throw new IndexOutOfBoundsException();
        }
        this.grid[row][column] = element;
        
        
    }

    @Override
    public CellState get(int row, int column) {
        if (row < 0 || row >= rows || column < 0 || column >= cols){
            throw new IndexOutOfBoundsException();
        }
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        return null;
    }
    
}
